# Cribl Role

## Role Variables

- `cribl_path`: Default Cribl path /opt
- `cribl_user`: Default user cribl
- `cribl_license`: Cribl license string, default ""
- `cribl_leader_host`:Cribl leader host, default leader.cribl.io
- `cribl_leader_port`: Cribl leader port, default 4200
- `cribl_leader_token`: Cribl authentication token, default criblmanager
- `cribl_worker_group`: Cribl worker group, default is default
- `cribl_worker`: Set host as Cribl worker, default is false
- `cribl_leader`: Set host as Cribl leader, default is true
- `cribl_tls`: Enable TLS between Cribl nodes, default is false
- `cribl_offline_install`: Use offline mode to install Cribl, default is false

## Example Playbook

    - hosts: leaders
    become: yes
    tasks:
        - import_role:
            name: cribl_ocd
        vars:
            cribl_leader: yes

    - hosts: workers
    become: yes
    tasks:
        - import_role:
            name: cribl_ocd
        vars:
            cribl_worker_group:
            - mygroup
            cribl_worker: yes
            cribl_leader_host: manager.ocd.local

## Author Information

Dauren Akilbekov dauren.akilbekov@orangecyberdefense.com

## License

[BSD 2-clause](LICENSE.txt)

## TODO

- Add NFS mounting for HA
- Add firewall ports
- Add documentation how to use role and write playbook
- Add installation of custom packs
- Add tests
- Add systemd
